#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of iperf3
#   Description: iPerf3 is a tool for active measurements of the maximum
#   achievable bandwidth on IP networks.
#   Author: Susant Sahani <susant@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2018 Red Hat, Inc.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="iperf3"
Iperf3PidFile="/var/run/iperf3-test-pid"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE

        if pgrep -x "iperf3" > /dev/null
        then
            rlLog "Iperf3 is already Running"
        fi

        rlRun "cp iperf3-test.py /usr/bin/"
    rlPhaseEnd

    rlPhaseStartTest
        rlLog "iperf3 tests"
        rlRun "/usr/bin/python3 /usr/bin/iperf3-test.py"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "rm /usr/bin/iperf3-test.py"

        if [ -f "$Iperf3PidFile" ]
        then
            rlRun "read Iperf3Pid < $Iperf3PidFile"
            rlRun "echo $Iperf3Pid"

            if [ -z $Iperf3Pid ]
            then
                rlRun "kill -9 $Iperf3Pid"
                rlRun "rm Iperf3PidFile"
            fi
        fi

        rlLog "iperf3 tests done"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd

rlGetTestState
